// public/core.js
var notifsManager = angular.module('notifsManager', []);

function mainController($scope, $http) {
    $scope.formData = {};

    // when landing on the page, get all cars and show them
    $http.get('/notif/')
        .success(function(data) {
            $scope.notifs = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the text to the node API
    $scope.createCar = function() {
        $http.post('/notif/', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
				
				$http.get('/notif/')
					.success(function(data) {
						$scope.notifs = data;
						console.log(data);
					})
					.error(function(data) {
						console.log('Error: ' + data);
					});
				
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
}