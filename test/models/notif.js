var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Notif = new Schema({

  device_id:    {
    type    : String,
    require : true
  },
  user_id:   {
    type: String,
    require : true
  },
  date_access:   {
    type: Date,
    default : true
  },
  ip_address :   {
    type    : Number,
    require : true
  },
  message: {
    type    : String,
    require : true
  },
  status: {
    type    : String,
    require : true
  }
});

Notif.path('device_id').validate(function (v) {
  return ((v != "") && (v != null));
});

module.exports = mongoose.model('Notif', Notif);