var Notif = require('../models/notif.js');

module.exports = function(app) {


  /**
   * Find and retrieves all notifications
   * @param {Object} req HTTP request object.
   * @param {Object} res HTTP response object.
   */
  findAllNotifs = function(req, res) {
    console.log("GET - /notifs");
    return Notif.find(function(err, cars) {
      if(!err) {
        return res.send(notifs);
      } else {
        res.statusCode = 500;
        console.log('Internal error(%d): %s',res.statusCode,err.message);
        return res.send({ error: 'Server error' });
      }
    });
  };



  /**
   * Find and retrieves a single notif by its ID
   * @param {Object} req HTTP request object.
   * @param {Object} res HTTP response object.
   */
  findById = function(req, res) {

    console.log("GET - /notif/:id");
    return Notif.findById(req.params.id, function(err, notif) {

      if(!notif) {
        res.statusCode = 404;
        return res.send({ error: 'Not found' });
      }

      if(!err) {
        return res.send({ status: 'OK', notif:notif });
      } else {

        res.statusCode = 500;
        console.log('Internal error(%d): %s', res.statusCode, err.message);
        return res.send({ error: 'Server error' });
      }
    });
  };




  /**
   * Creates a new notification from the data request
   * @param {Object} req HTTP request object.
   * @param {Object} res HTTP response object.
   */
  addNotif = function(req, res) {

    console.log('POST - /notif');

    var notif = new Notif({
      device_id	:    req.body.device_id,
      user_id	:    req.body.user_id,
      date-access:   req.body.date_access,
      ip_address:    req.body.ip_address,
	  message 	:	 req.body.message,
	  status	:	 req.body.status
    });

    notif.save(function(err) {

      if(err) {

        console.log('Error while saving notif: ' + err);
        res.send({ error:err });
        return;

      } else {

        console.log("Notification created");
        return res.send({ status: 'OK', notif:notif });

      }

    });

  };

  //Link routes and actions
  app.get('/notif', findAllNotifs);
  app.get('/notif/:id', findById);
  app.post('/notif', addNotif);

}